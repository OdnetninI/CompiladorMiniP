#include "ts.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct CubetaSIM ** tablaSimbolos;
struct CubetaSTR ** tablaStr;
int tablaSimbolos_cub = CUBETA_SIM_START;
int tablaStr_cub = CUBETA_STR_START;
int tablaSimbolos_num = 0;
int tablaStr_num = 0;

void iniciarTablas () {
  tablaSimbolos = malloc(tablaSimbolos_cub*sizeof(struct CubetaSIM*));
  for (int i = 0; i < tablaSimbolos_cub; i++)
    tablaSimbolos[i] = nullptr;
  tablaStr = malloc(tablaStr_cub*sizeof(struct CubetaSTR*));
  for (int i = 0; i < tablaStr_cub; i++)
      tablaStr[i] = nullptr;
}

long hash (const char* key) {
  long m = 1;
  long h = 0;
  for (int i = 0; i < strlen(key); i++) {
    h = h + key[i] * m;
    m *= 10;
  }
  return h;
}

void insertarEnTablaSimbolos (const char* id) {
  long h =  hash(id) % tablaSimbolos_cub;
  if (tablaSimbolos[h] != nullptr) {
    struct CubetaSIM * aux = tablaSimbolos[h];
    struct CubetaSIM * antAux = nullptr;
    while (aux != nullptr && strcmp(aux->id, id) < 0) {
      antAux = aux;
      aux = aux->next;
    }

    if (aux == nullptr) {
      struct CubetaSIM * nueva = malloc(sizeof(struct CubetaSIM));
      nueva->id = strdupe(id);
      nueva->next = nullptr;
      antAux->next = nueva;
    }
    else {
      if (strcmp(aux->id, id) != 0) {
        struct CubetaSIM * nueva = malloc(sizeof(struct CubetaSIM));
        nueva->id = strdupe(id);
        nueva->next = aux->next;
        aux->next = nueva;
      }
      else ; // Se ha encontrado
    }
  }
  else {
    tablaSimbolos[h] = malloc(sizeof(struct CubetaSIM));
    tablaSimbolos[h]->id = strdupe(id);
    tablaSimbolos[h]->next = nullptr;
  }
}

uint16_t insertarEnTablaCadenas(const char* string, uint16_t num) {
  long h =  hash(string) % tablaStr_cub;
  if (tablaStr[h] != nullptr) {
    struct CubetaSTR * aux = tablaStr[h];
    struct CubetaSTR * antAux = nullptr;
    while (aux != nullptr && strcmp(aux->text, string) < 0) {
      antAux = aux;
      aux = aux->next;
    }

    if (aux == nullptr) {
      struct CubetaSTR * nueva = malloc(sizeof(struct CubetaSTR));
      nueva->text = strdupe(string);
      nueva->next = nullptr;
      nueva->num = num;
      antAux->next = nueva;
      return num;
    }
    else {
      if (aux == tablaStr[h] && strcmp(aux->text, string) != 0) {
        struct CubetaSTR * nueva = malloc(sizeof(struct CubetaSTR));
        nueva->text = strdupe(string);
        nueva->next = aux;
        nueva->num = num;
        tablaStr[h] = nueva;
        return num;
      }

      else if (strcmp(aux->text, string) != 0) {
        struct CubetaSTR * nueva = malloc(sizeof(struct CubetaSTR));
        nueva->text = strdupe(string);
        nueva->next = aux->next;
        nueva->num = num;
        aux->next = nueva;
        return num;
      }
      else return aux->num; // Se ha encontrado
    }
  }
  else {
    tablaStr[h] = malloc(sizeof(struct CubetaSTR));
    tablaStr[h]->text = strdupe(string);
    tablaStr[h]->num = num;
    tablaStr[h]->next = nullptr;
    return num;
  }
  return 0xFFFF;
}

bool isDeclared (const char* id) {
  long h =  hash(id) % tablaSimbolos_cub;
  if (tablaSimbolos[h] != nullptr) {
    struct CubetaSIM * aux = tablaSimbolos[h];
    struct CubetaSIM * antAux = nullptr;
    while (aux != nullptr && strcmp(aux->id, id) < 0) {
      antAux = aux;
      aux = aux->next;
    }

    if (aux == nullptr) return false;
    else {
      if (strcmp(aux->id, id) == 0) return true;
      else return false;
    }
  }
  else return false;
}

void destruirTablas () {
  for (int i = 0; i < tablaSimbolos_cub; i++) {
    struct CubetaSIM * aux = tablaSimbolos[i];
    struct CubetaSIM * antAux = nullptr;
    while (aux != nullptr) {
      antAux = aux;
      aux = aux->next;
      free(antAux->id);
      free(antAux);
    }
  }
  free (tablaSimbolos);

  for (int i = 0; i < tablaStr_cub; i++) {
    struct CubetaSTR * aux = tablaStr[i];
    struct CubetaSTR * antAux = nullptr;
    while (aux != nullptr) {
      antAux = aux;
      aux = aux->next;
      free(antAux->text);
      free(antAux);
    }
  }
  free (tablaStr);
}

void imprimirTablas () {
  printf("\t.data\n");
  printf("\n# Cadenas\n");
  for (int i = 0; i < tablaStr_cub; i++) {
    struct CubetaSTR * aux = tablaStr[i];
    while (aux != nullptr) {
      printf("$str%d:\n\t.asciiz %s\n", aux->num,aux->text);
      aux = aux->next;
    }
  }

  printf("\n# Variables\n");
  for (int i = 0; i < tablaSimbolos_cub; i++) {
    struct CubetaSIM * aux = tablaSimbolos[i];
    while (aux != nullptr) {
      printf("_%s:\n\t.word 0\n", aux->id);
      aux = aux->next;
    }
  }
}
