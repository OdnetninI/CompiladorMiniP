%{
#include <stdio.h>
#include <stdlib.h>
#include "ts.h"
#include "codigo.h"
#include "errors.h"
extern int yylex(void);
extern int yylineno;
void yyerror (const char *msg);
void syntacticError (int, char*);
void undeclared (char *);
void redeclared (char* );
extern char* yytext;
uint16_t strs = 0;
uint16_t etqsIF = 0;
uint16_t etqsLOOP = 0;

#define nullptr NULL
#define ERROR_COLOR "\x1B[31m"
#define WARNING_COLOR "\x1B[33m"
#define CLEAN_COLOR "\x1B[37m"
%}

%union {
	char* cad;
	struct listaCodigo* aes;
}
%token <cad> ID STRINGS NUMBERS
%type <aes> expression statement compound_statement program statement_list optional_statements print_item print_list read_list
%token COMA SEMICOMA POINT DOUBLEPOINT LP RP ID_ERROR
%token ID_SI ID_THEN ID_IFNO ID_WHILE ID_DO ID_PROGRAM ID_VAR ID_INT ID_START ID_END ID_PRINT
%token ID_READ ID_MAIN ID_FALSE ID_TRUE ID_FOR ID_HASTA
%right ASSING
%left MINUS PLUS
%left MULTI DIV
%left MIN

%%
program : {iniciarTablas();}
					ID_PROGRAM ID LP RP SEMICOMA declarations compound_statement POINT
					{imprimirTablas(); destruirTablas();
					// Code print
					printf ("\n\t.text\n\t.globl main\n\nmain:\n");
					$$ = $8;
					anadirCodigo(true, $$, "jr", "$ra", nullptr, nullptr);
					printCode($$);
					liberarCode($$);
				}
				;

declarations : declarations ID_VAR identifier_list DOUBLEPOINT type SEMICOMA {}
							| {};

identifier_list : ID {if (isDeclared($1)) redeclared($1);
									insertarEnTablaSimbolos($1);}
								| identifier_list COMA ID {if (isDeclared($3)) redeclared($3);
										insertarEnTablaSimbolos($3);};

type : ID_INT {};

compound_statement : ID_START optional_statements ID_END {$$ = $2;};

optional_statements : statement_list {$$ = $1;}
										| {$$ = iniciar();};

statement_list : statement {$$ = $1;}
								| statement_list SEMICOMA statement {
										$$ = $1;
										mergeCode(true, $$, $3);
									}
								| statement_list error statement {
										$$ = $1;
										mergeCode(true, $$, $3);
										syntacticError(EXPECTED_X_ERROR, "\';\'");
									}
								;

statement : ID ASSING expression {
							$$ = $3;
							if (!isDeclared($1)) undeclared($1);
							char t[18];
							sprintf(t, "_%s", $1);
							anadirCodigo(true, $$, "sw", $$->fin->code->reg1, t, nullptr);
							liberarTemporal($$->fin->code->reg1);
						}
					| ID ASSING error {
							$$ = iniciar();
							if (!isDeclared($1)) undeclared($1);
							syntacticError(EXPECTED_X_ERROR, "an expression");
						}
					| compound_statement {$$ = $1;}
					| ID_SI expression ID_THEN statement ID_IFNO statement {$$ = $2;
							char etq[32];
							char etq2[32];
							sprintf(etq, "IF%d", etqsIF);
							etqsIF++;
							sprintf(etq2, "IF%d", etqsIF);
							etqsIF++;

							anadirCodigo(true, $$, "beqz", $$->fin->code->reg1, etq, nullptr);
							liberarTemporal($$->fin->code->reg1);
							mergeCode(true, $$, $4);
							sprintf(etq, "%s:", etq);
							anadirCodigo(true, $$, "b", etq2, nullptr, nullptr);
							anadirCodigo(true, $$, etq, nullptr, nullptr, nullptr);
							mergeCode(true, $$, $6);
							sprintf(etq2, "%s:", etq2);
							anadirCodigo(true, $$, etq2, nullptr, nullptr, nullptr);
						}
					| ID_SI error ID_THEN statement ID_IFNO statement {$$ = $4;
							mergeCode(true, $$, $6);
							syntacticError(EXPECTED_X_ERROR, "an expression at if condition previous");
						}
					| ID_SI expression ID_THEN statement {
							$$ = $2;
							char etq[32];
							sprintf(etq, "IF%d", etqsIF);
							etqsIF++;
							anadirCodigo(true, $$, "beqz", $$->fin->code->reg1, etq, nullptr);
							liberarTemporal($$->fin->code->reg1);
							mergeCode(true, $$, $4);
							sprintf(etq, "%s:", etq);
							anadirCodigo(true, $$, etq, nullptr, nullptr, nullptr);
						}
					| ID_WHILE expression ID_DO statement {
							$$ = $2;
							char etq[32];
							sprintf(etq, "LOOP%d", etqsLOOP);
							etqsLOOP++;
							char etq2[32];
							sprintf(etq2, "LOOP%d", etqsLOOP);
							etqsLOOP++;
							char etq3[32];
							anadirCodigo(true, $$, "beqz", $$->fin->code->reg1, etq2, nullptr);
							liberarTemporal($$->fin->code->reg1);
							sprintf(etq3, "%s:", etq);
							anadirCodigo(false, $$, etq3, nullptr, nullptr, nullptr);
							mergeCode(true, $$, $4);
							anadirCodigo(true, $$, "b", etq, nullptr, nullptr);
							sprintf(etq2, "%s:", etq2);
							anadirCodigo(true, $$, etq2, nullptr, nullptr, nullptr);
						}
					| ID_PRINT print_list {$$ = $2;}
					| ID_READ read_list {$$ = $2;}
					| ID_DO statement ID_WHILE expression {
							$$ = $2;
							char etq[32];
							sprintf(etq, "LOOP%d", etqsLOOP);
							etqsLOOP++;
							mergeCode(true, $$, $4);
							anadirCodigo(true, $$, "bnez", $$->fin->code->reg1, etq, nullptr);
							sprintf(etq, "%s:", etq);
							anadirCodigo(false, $$, etq, nullptr, nullptr, nullptr);
						}
					| ID_FOR ID ASSING expression ID_HASTA expression ID_DO statement {
							$$ = $4;
							if (!isDeclared($2)) undeclared($2);
							char t[18];
							sprintf(t, "_%s", $2);
							anadirCodigo(true, $$, "sw", $$->fin->code->reg1, t, nullptr);
							liberarTemporal($$->fin->code->reg1);

							char etq[32], etq2[32], etq3[32];
							sprintf(etq, "LOOP%d", etqsLOOP);
							etqsLOOP++;
							sprintf(etq3, "LOOP%d", etqsLOOP);
							etqsLOOP++;

							sprintf(etq2, "%s:", etq);
							anadirCodigo(true, $$, etq2, nullptr, nullptr, nullptr);

							mergeCode(true, $$, $6);
							char* registro = $$->fin->code->reg1;
							anadirCodigo(true, $$, "lw", nuevoTemporal(), t, nullptr);
							liberarTemporal($$->fin->code->reg1);
							anadirCodigo(true, $$, "sub", nuevoTemporal(), $$->fin->code->reg1, registro);
							anadirCodigo(true, $$, "beqz", $$->fin->code->reg1, etq3, nullptr);
							liberarTemporal($$->fin->code->reg1);
							liberarTemporal(registro);
							mergeCode(true, $$, $8);

							anadirCodigo(true, $$, "lw", nuevoTemporal(), t, nullptr);
							liberarTemporal($$->fin->code->reg1);
							anadirCodigo(true, $$, "addi", nuevoTemporal(), $$->fin->code->reg1, "1");
							liberarTemporal($$->fin->code->reg1);
							anadirCodigo(true, $$, "sw", $$->fin->code->reg1, t, nullptr);

							anadirCodigo(true, $$, "b", etq, nullptr, nullptr);
							sprintf(etq2, "%s:", etq3);
							anadirCodigo(true, $$, etq2, nullptr, nullptr, nullptr);
						}
						| ID_SI error ID_THEN statement {
								$$ = $4;
								syntacticError(EXPECTED_X_ERROR, "an expression");
							}
					;

print_list : print_item {$$ = $1;}
					| print_list COMA print_item {
						$$ = $1;
						mergeCode(true, $$, $3);
					}

print_item : expression {
							$$ = $1;
							char* registro = $$->fin->code->reg1;
							liberarTemporal(registro);
							anadirCodigo(true, $$, "li", "$v0", "1", nullptr);
							anadirCodigo(true, $$, "move", "$a0", registro, nullptr);
							anadirCodigo(true, $$, "syscall", nullptr, nullptr, nullptr);
						}
					| STRINGS {uint16_t x = insertarEnTablaCadenas($1, strs); if (x >= strs) strs++;
							$$ = iniciar();
							char cadena[32];
							sprintf(cadena, "$str%d", x);
							anadirCodigo(true, $$, "li", "$v0", "4", nullptr);
							anadirCodigo(true, $$, "la", "$a0", cadena, nullptr);
							anadirCodigo(true, $$, "syscall", nullptr, nullptr, nullptr);
						};

read_list : ID {if (!isDeclared($1)) undeclared($1);
							$$ = iniciar();
							anadirCodigo(true, $$, "li", "$v0", "5", nullptr);
							anadirCodigo(true, $$, "syscall", nullptr, nullptr, nullptr);
							char t[18];
							sprintf(t, "_%s", $1);
							anadirCodigo(true, $$, "sw", "$v0", t, nullptr);
						}
					| read_list COMA ID {if (!isDeclared($3)) undeclared($3);
							$$ = $1;
							anadirCodigo(true, $$, "li", "$v0", "5", nullptr);
							anadirCodigo(true, $$, "syscall", nullptr, nullptr, nullptr);
							char t[18];
							sprintf(t, "_%s", $3);
							anadirCodigo(true, $$, "sw", "$v0", t, nullptr);
						}
					| read_list error ID {if (!isDeclared($3)) undeclared($3);
							$$ = $1;
							anadirCodigo(true, $$, "li", "$v0", "5", nullptr);
							anadirCodigo(true, $$, "syscall", nullptr, nullptr, nullptr);
							char t[18];
							sprintf(t, "_%s", $3);
							anadirCodigo(true, $$, "sw", "$v0", t, nullptr);
							syntacticError(EXPECTED_X_ERROR,"\',\'");
						}
						;

expression : expression PLUS expression {$$ = $3;
								liberarTemporal($1->fin->code->reg1);
								liberarTemporal($3->fin->code->reg1);
								mergeCode(false, $$, $1);
								anadirCodigo(true, $$, "add", nuevoTemporal(), $1->fin->code->reg1, $3->fin->code->reg1);
							}
					| expression MINUS expression {$$ = $3;
								liberarTemporal($1->fin->code->reg1);
								liberarTemporal($3->fin->code->reg1);
								mergeCode(false, $$, $1);
								anadirCodigo(true, $$, "sub", nuevoTemporal(), $1->fin->code->reg1, $3->fin->code->reg1);
							}
					| expression MULTI expression {$$ = $3;
								liberarTemporal($1->fin->code->reg1);
								liberarTemporal($3->fin->code->reg1);
								mergeCode(false, $$, $1);
								anadirCodigo(true, $$, "mul", nuevoTemporal(), $1->fin->code->reg1, $3->fin->code->reg1);
							}
					| expression DIV expression {$$ = $3;
								liberarTemporal($1->fin->code->reg1);
								liberarTemporal($3->fin->code->reg1);
								mergeCode(false, $$, $1);
								anadirCodigo(true, $$, "div", nuevoTemporal(), $1->fin->code->reg1, $3->fin->code->reg1);
							}
					| MINUS expression %prec MIN {$$ = $2;
								anadirCodigo(true, $$, "neg", $2->fin->code->reg1, $2->fin->code->reg1, nullptr);
							}
					| LP expression RP {$$ = $2;}
					| ID {
								if (!isDeclared($1)) undeclared($1);
								$$ = iniciar();
								char t[18];
								sprintf(t, "_%s", $1);
								anadirCodigo(false, $$, "lw", nuevoTemporal(), t, nullptr);
							}
					| NUMBERS {
								$$ = iniciar();
								anadirCodigo(false, $$, "li",  nuevoTemporal(), $1, nullptr);
							};

%%
void yyerror (const char*msg) {
  //printf("%s _ %s %d\n", msg, yytext, yylineno);
}

void syntacticError (int opcode, char* arg) {
	switch (opcode) {
		case EXPECTED_X_ERROR:
			printf("%sERROR: Expected %s at line %d.", ERROR_COLOR, arg, yylineno);
			break;
		case NO_PROGRAM_ERROR:
			printf("%sERROR: All program must have a \'program\' declaration at begin.", ERROR_COLOR);
			break;
	}

	printf("%s\n", CLEAN_COLOR);
}

void undeclared (char * id) {
	printf("%sERROR: \'%s\' varaible is undeclared.", ERROR_COLOR, id);
	printf("%s\n", CLEAN_COLOR);
}

void redeclared (char* id) {
	printf("%sWARNING: Variable \'%s\' is already declared.", WARNING_COLOR, id);
	printf("%s\n", CLEAN_COLOR);
}
