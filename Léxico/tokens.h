#ifndef __TOKENS_H__
#define __TOKENS_H__

#define			ID						0x01
#define			NUMBERS				0x02
#define			STRINGS				0x03
#define			COMA					0x04
#define			SEMICOMA			0x05
#define			POINT					0x06
#define			DOUBLEPOINT		0x07
#define			PLUS					0x08
#define			MINUS					0x09
#define			MULTI					0x0A
#define			ASSING				0x0B
#define			LP						0x0C
#define			RP						0x0D

// Token de error, interesante para ciertas cosas
#define			ERROR					0xFF

#define ReservedWords 15

#endif // __TOKENS_H__

