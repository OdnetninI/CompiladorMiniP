#include "codigo.h"
#include "functions.h"
#include <stdio.h>

Codigo iniciar () {
  Codigo code = malloc(sizeof(struct listaCodigo));
  code->inicio = nullptr;
  code->fin = nullptr;
  return code;
}

void anadirCodigo (bool fin, Codigo code, const char* op, const char* reg1, const char* reg2, const char* reg3) {
  struct nodoCodigo* line = malloc(sizeof(struct nodoCodigo));
  line->code = malloc(sizeof(struct _codigo));
  line->next = nullptr;
  if (!fin) line->next = code->inicio;

  line->code->op = strdupe(op);
  line->code->reg1 = nullptr;
  line->code->reg2 = nullptr;
  line->code->reg3 = nullptr;
  if (reg1 != nullptr) line->code->reg1 = strdupe(reg1);
  if (reg2 != nullptr) line->code->reg2 = strdupe(reg2);
  if (reg3 != nullptr) line->code->reg3 = strdupe(reg3);

  if (!fin) {
    code->inicio = line;
    if (code->fin == nullptr) {
      code->fin = line;
    }
  }
  else {
    if (code->fin != nullptr) code->fin->next = line;
    code->fin = line;
    if (code->inicio == nullptr)
      code->inicio = line;
  }
}

void mergeCode (bool fin, Codigo total, Codigo aux) {
  if (fin) {
    if (total->fin != nullptr) total->fin->next = aux->inicio;
    else total->inicio = aux->inicio;
    total->fin = aux->fin;
  }
  else {
    aux->fin->next = total->inicio;
    total->inicio = aux->inicio;
    if(total->fin == nullptr) total->fin = aux->fin;
  }
  free(aux);
}

bool temporales[10] = {false,false,false,false,false,false,false,false,false,false};

char* nuevoTemporal () {
  for (int i = 0; i < 10; i++) {
    if (!temporales[i]) {
      temporales[i] = true;
      char* temp = malloc(sizeof(char)*4);
      sprintf(temp, "$t%d", i);
      return temp;
    }
  }
  return nullptr;
}

void liberarTemporal (const char * x) {
  temporales[x[2]-'0'] = false;
}

void printCode (Codigo code) {
  struct nodoCodigo* aux = code->inicio;
  while (aux != nullptr) {
    struct _codigo* _code = aux->code;
    printf("\t%s", _code->op);
    if (_code->reg1 != nullptr) {
      printf(" %s", _code->reg1);
      if (_code->reg2 != nullptr) {
        printf(", %s", _code->reg2);
        if (_code->reg3 != nullptr) {
          printf(", %s", _code->reg3);
        }
      }
    }
    printf("\n");
    aux = aux->next;
  }
}

void liberarCode (Codigo code) {
  struct nodoCodigo* aux = code->inicio;
  struct nodoCodigo* borrar;
  while (aux != nullptr) {
    borrar = aux;
    aux = aux->next;
    free(borrar->code->op);
    free(borrar->code->reg1);
    free(borrar->code->reg2);
    free(borrar->code->reg3);
    free(borrar->code);
    free(borrar);
  }
  free (code);
}
