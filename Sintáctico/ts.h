#ifndef __TS_H__
#define __TS_H__

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include "functions.h"
#define nullptr NULL

#define CUBETA_SIM_START 10
struct CubetaSIM {
  char* id;
  struct CubetaSIM* next;
};

#define CUBETA_STR_START 15
struct CubetaSTR {
  char* text;
  uint16_t num;
  struct CubetaSTR* next;
};


void imprimirTablas ();
void iniciarTablas ();
void insertarEnTablaSimbolos (const char* id);
bool isDeclared (const char* id);
uint16_t insertarEnTablaCadenas(const char* string, uint16_t);
void destruirTablas ();

#endif // __TS_H__
