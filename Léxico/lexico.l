%option yylineno
%{
	#include "tokens.h"
	#include "functions.h"
	#include "errors.h"
	#include <stdlib.h>
	#include <string.h>

	void lexicalError(int error);

	#define ERROR_COLOR "\x1B[31m"
	#define WARNING_COLOR "\x1B[33m"
	#define CLEAN_COLOR "\x1B[37m"
	
	/*char* tablaReserved[ReservedWords] = {"si","entonces","si-no","mientras","hacer","programa","var","entero","comienzo","fin",
																				"imprimir","leer","main","false","true"};
	char** tablaSimbolos;*/
	int n_simbolos = 0;
%}
%%
	/* Comentarios y espacios varios */
(\{[^\}\n]*\}|\(\*([^\*]|\*+[^\)])*\*+\))|((\t)|(\s)|(\n)|(" "))*	;
	/* Identificadores */
si	return ID;
entonces	return ID;
si-no	return ID;
mientras	return ID;
hacer	return ID;
programa	return ID;
var	return ID;
entero	return ID;
comienzo	return ID;
fin	return ID;
imprimir	return ID;
leer	return ID;
main	return ID;
false	return ID;
true	return ID;
[a-zA-Z_][a-zA-Z_0-9]*	return identificatorParser();
	/* Numeros */
[0-9]+									return numberParser();
;	return SEMICOMA;
\.	return POINT;
:	return DOUBLEPOINT;
,	return COMA;
\+	return PLUS;
-	return MINUS;
\* return MULTI;
:= return ASSING;
\(	return LP;
\)	return RP;
\"((\\\")|(\\\\)|[^"])*\"	return STRINGS;
\"((\\\")|(\\\\)|[^"\n])*	lexicalError(NO_CLOSED_STRING); return ERROR;
(\{[^\}\n]*)|([^\}\n]*\}) lexicalError(UNBALANCED_BRACKETS); return ERROR;
[^a-zA-Z;,.*+\-:()0-9"{} \n\t]+	lexicalError(INVALID_CHARACTER); return ERROR;
%%

void lexicalError (int error) {
	switch(error) {
		case ID_LENGTH_ERROR:
			printf("%sWarning: Too long Identifier name at line %d \"%s\", length %d, max length is 16. ",WARNING_COLOR, yylineno, yytext, (int)yyleng);
			yytext[16] = '\0';
			printf("It has been trunc to \"%s\".", yytext);
			break;

		case NUMBER_LIMIT_ERROR:
			printf ("%sERROR: Number out of Range [-2147483648, 2147483648]  \"%s\" at line %d.", ERROR_COLOR, yytext, yylineno);
			break;

		case INVALID_CHARACTER:
			printf("%sERROR: Invalid character/s \"%s\" at line %d.", ERROR_COLOR, yytext, yylineno);
			break;

		case NO_CLOSED_STRING:
			printf("%sERROR: Missing \'\"\' to close string at line %d.", ERROR_COLOR, yylineno);
			break;

		case UNBALANCED_BRACKETS:
			printf("%sERROR: Unbalanced Brackets at line %d.", ERROR_COLOR, yylineno);
			break;

		default:
			printf("%sERROR: Unknown error ID \"%d\" at line %d. ", ERROR_COLOR, error, yylineno);
			printf("Error analyzing \"%s\" with legth %d", yytext, (int)yyleng);
			break;
	}

	printf("%s\n", CLEAN_COLOR);
}

int identificatorParser () {
	if (yyleng > 16) { 
		lexicalError (ID_LENGTH_ERROR);
		yytext[16] = '\0';
		yyleng = 16;
	}
	/*if (n_simbolos == 0) {tablaSimbolos = malloc(sizeof(char*)); n_simbolos = 1; tablaSimbolos[0] = calloc(sizeof(char), 17);}
	int j = 0;
	for (; j < n_simbolos && strcmp(yytext,tablaSimbolos[j]) != 0; j++);
	if (j != n_simbolos) sprintf(yytext, "%d", j+ReservedWords);
	else {
		tablaSimbolos = realloc (tablaSimbolos, sizeof(char*)*n_simbolos+1);
		tablaSimbolos[n_simbolos] = calloc(sizeof(char), 17);
		sprintf(tablaSimbolos[n_simbolos], "%s", yytext);
		n_simbolos++;
		sprintf(yytext, "%d", j+ReservedWords);
	}*/
	return ID;
}

int numberParser() {
	long long num = atoll(yytext);
	if (num > 2147483648 || num < -2147483648) {
		lexicalError(NUMBER_LIMIT_ERROR);
		return ERROR;
	}
	return NUMBERS;
}

