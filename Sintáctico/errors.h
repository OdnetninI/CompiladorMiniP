#ifndef __ERRORS_H__
#define __ERRORS_H__

#define		ID_LENGTH_ERROR			0x01
#define		NUMBER_LIMIT_ERROR	0x02
#define		INVALID_CHARACTER		0x03
#define		NO_CLOSED_STRING		0x04
#define		UNBALANCED_BRACKETS	0x05

#define		LEXICAL_ERROR				0xFF

#define   EXPECTED_X_ERROR  0x01
#define   NO_PROGRAM_ERROR  0x02

#endif // __ERRORS_H__
