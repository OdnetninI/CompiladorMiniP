#ifndef __CODIGO_H__
#define __CODIGO_H__

#include <stdlib.h>
#include <stdbool.h>
#define nullptr NULL

struct listaCodigo {
  struct nodoCodigo* inicio;
  struct nodoCodigo* fin;
};

struct nodoCodigo {
  struct _codigo* code;
  struct nodoCodigo* next;
};

struct _codigo {
  char* op;
  char* reg1;
  char* reg2;
  char* reg3;
};

typedef struct listaCodigo* Codigo;


Codigo iniciar ();
void anadirCodigo (bool fin, Codigo code, const char* op, const char* reg1, const char* reg2, const char* reg3);
void mergeCode (bool fin, Codigo total, Codigo aux);
void printCode (Codigo code);
char* nuevoTemporal ();
void liberarTemporal (const char * x);
void liberarCode (Codigo code);

#endif // __CODIGO_H__
